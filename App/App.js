/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Alert
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const App: () => React$Node = () => {
  var base = "http://example.org"; 
  const createAlert = (title, body) => {
      Alert.alert(
      title, 
      body
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log("OK Pressed") }
      ],
      { cancelable: false }
    );
  }
  const onPressGet = () => {
    fetch(`${base}/resource/get`).then((response) => {
      if (response.status !== 200) {
        createAlert('Looks like there was a problem. Status Code: ' +
          response.status);
        return;
      }
      createAlert('Success!');
    });
  }

  const onPressSet = () => {
    fetch(`${base}/resource/set`,{
     method: 'POST',
  headers: {
    Accept: 'text/plain',
    'Content-Type': 'application/x-www-form-urlencoded'
  },
  body: JSON.stringify({
    value: '10'
  })
} 
    ).then((response) => {
      if (response.status !== 200) {
        createAlert('Looks like there was a problem. Status Code: ' +
          response.status);
        return;
      }
      createAlert('Success!');
    });

  }
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <Header />
          {global.HermesInternal == null ? null : (
            <View style={styles.engine}>
              <Text style={styles.footer}>Engine: Hermes</Text>
            </View>
          )}
          <View style={styles.body}>
            <Button
            style={styles.button}
            onPress={onPressGet}
            title="Get Resource"
            color="#841584"
            accessibilityLabel="Call the GET"
          />
          <Button
            style={styles.button}
            onPress={onPressSet}
            title="Set Resource"
            color="#841584"
            accessibilityLabel="Call the POST"
          />

          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  button: {
    marginBottom: 10
  },
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
